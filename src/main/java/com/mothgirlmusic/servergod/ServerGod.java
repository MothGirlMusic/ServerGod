package com.mothgirlmusic.servergod;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;

public class ServerGod extends JavaPlugin {

    @Override
    public void onEnable() {
        PluginDescriptionFile pdffile = getDescription();
        getLogger().info(pdffile.getName() + " version " + pdffile.getVersion() + " now enabled.");
    }
    @Override
    public void onDisable() {
        PluginDescriptionFile pdffile = getDescription();
        getLogger().info(pdffile.getName() + " version " + pdffile.getVersion() + " now disabled.");
    }

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (cmd.getName().equalsIgnoreCase("hello") && sender instanceof Player) {
            Player player = (Player) sender;

            player.sendMessage("Hello, " + player.getName() + "!");

            return true;
        }
 
        return false;
    }

}
